#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/time.h>
#include <arpa/inet.h>

#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <net/if.h>
#include <netdb.h>

#include <sys/socket.h>
#include <netinet/in.h>

static volatile int keepRunning = 1;

void intHandler(int dummy)
{
	keepRunning = 0;
	exit(0);
}

long getMicrotimeLong(void)
{
	struct timeval currentTime;
	gettimeofday(&currentTime, NULL);
	return currentTime.tv_sec * (int)1e6 + currentTime.tv_usec;
}

int open_socket(char* port, char* iface)
{
	const char* hostname=0; /* wildcard */
	struct addrinfo hints;
	memset(&hints,0,sizeof(hints));
	hints.ai_family=AF_UNSPEC;
	hints.ai_socktype=SOCK_DGRAM;
	hints.ai_protocol=0;
	hints.ai_flags=AI_PASSIVE|AI_ADDRCONFIG;
	struct addrinfo* res=0;
	int err=getaddrinfo(hostname,port,&hints,&res);
	if (err!=0) {
		printf("failed to resolve local socket address (err=%d)",err);
	}

	int fd=socket(res->ai_family,res->ai_socktype,res->ai_protocol);
	if (fd==-1) {
		printf("%s",strerror(errno));
	}
	if (bind(fd,res->ai_addr,res->ai_addrlen)==-1) {
		printf("%s",strerror(errno));
	}

	if (strcmp(iface, "") != 0) {

		struct ifreq ifr;

		memset(&ifr, 0, sizeof(ifr));
		snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), iface);
		if (setsockopt(fd, SOL_SOCKET, SO_BINDTODEVICE, (void *)&ifr, sizeof(ifr)) < 0) {
			perror("error binding socket to interface");
		}
	}

	freeaddrinfo(res);
	return fd;
}

int receive_udp(char* port, int respond,  char* iface)
{
	int fd = open_socket(port, iface);

	char buffer[4100];
	struct sockaddr_storage src_addr;
	socklen_t src_addr_len=sizeof(src_addr);

	long lastReportTime = getMicrotimeLong();
	long resBytes = 0;
	long resPackages = 0;
	int interval = (int)1e6;
	
	long avg_pkg_size = (resBytes / resPackages);

	while (keepRunning) {

		long currTime = getMicrotimeLong();
		if (currTime - lastReportTime > interval) {
			if (currTime - lastReportTime > 2 * interval) {
				lastReportTime = currTime;
				resBytes = 0;
				resPackages = 0;
				continue;
			}
			lastReportTime += interval;
			
			if (avg_pkg_size != resBytes / resPackages) {
				printf("\n");
			}
			
			avg_pkg_size = (resBytes / resPackages);
			printf("%f Mpps, and %f GBps. Detail: ", resPackages / (double)1e6, resBytes * 8 / (double)1e9);
			printf("%ld packages and %ld bytes received. avg pkg size:%ld.\n", resPackages, resBytes, avg_pkg_size);

			resBytes = 0;
			resPackages = 0;
		}

		for(int i = 0; i < 100; i++) {
			ssize_t count = recvfrom(fd,buffer,sizeof(buffer),0,(struct sockaddr*)&src_addr,&src_addr_len);
			if (respond) {
				// send the received package back to the sender
				if (sendto(fd, buffer, count, 0, (struct sockaddr*)&src_addr, (socklen_t) src_addr_len) == -1) {
					printf("error sending package back to client!");
				}
			}
			if (count==-1) {
				printf("%s",strerror(errno));
			} else if (count==sizeof(buffer)) {
				printf("datagram too large for buffer: truncated");
			} else {
				resBytes += (long)count;
				resPackages++;
			}
		}

	}
	return 0;
}


int receive_tcp(char* port, int respond, char* iface)
{
	// port to start the server on
	int SERVER_PORT = 1234;
	if(sscanf (port, "%i", &SERVER_PORT) != 1) {
		fprintf(stderr, "error - not an integer");
		SERVER_PORT = 1234;
	}

	// socket address used for the server
	struct sockaddr_in server_address;
	memset(&server_address, 0, sizeof(server_address));
	server_address.sin_family = AF_INET;

	// htons: host to network short: transforms a value in host byte
	// ordering format to a short value in network byte ordering format
	server_address.sin_port = htons(SERVER_PORT);

	// htonl: host to network long: same as htons but to long
	server_address.sin_addr.s_addr = htonl(INADDR_ANY);

	// create a TCP socket, creation returns -1 on failure
	int listen_sock;
	if ((listen_sock = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		printf("could not create listen socket\n");
		return 1;
	}

	// bind it to listen to the incoming connections on the created server
	// address, will return -1 on error
	if ((bind(listen_sock, (struct sockaddr *)&server_address,
	          sizeof(server_address))) < 0) {
		printf("could not bind socket\n");
		return 1;
	}

	int wait_size = 16;  // maximum number of waiting clients, after which
	// dropping begins
	if (listen(listen_sock, wait_size) < 0) {
		printf("could not open socket for listening\n");
		return 1;
	}

	// socket address used to store client address
	struct sockaddr_in client_address;
	int client_address_len = 0;

	long lastReportTime = getMicrotimeLong();
	long resBytes = 0;
	long resPackages = 0;

	int interval = (int)1e6;

	// run indefinitely
	while (keepRunning) {

		// open a new socket to transmit data per connection
		int sock;
		if ((sock =
		         accept(listen_sock, (struct sockaddr *)&client_address,
		                (socklen_t *)&client_address_len)) < 0) {
			printf("could not open a socket to accept data\n");
			return 1;
		}

		int n = 0;
		int len = 0, maxlen = 2049;
		char buffer[maxlen];
		char *pbuffer = buffer;

		printf("client connected with ip address: %p\n",
		       inet_ntoa(client_address.sin_addr));

		// keep running as long as the client keeps the connection open
		while ((n = recv(sock, pbuffer, sizeof buffer, 0)) > 0) {
//			pbuffer += n;
			len += n;

			resBytes += n;
			resPackages++;

			if (respond) {
				// echo received content back
				send(sock, buffer, n, 0);
			}
			long currTime = getMicrotimeLong();
			if (currTime - lastReportTime > interval) {
				if (currTime - lastReportTime > 2 * interval) {
					lastReportTime = currTime;
					resBytes = 0;
					resPackages = 0;
					continue;
				}
				lastReportTime += interval;
				printf("%f Mpps, and %f GBps. Detail: ", resPackages / (double)1e6, resBytes * 8 / (double)1e9);
				printf("%ld packages and %ld bytes received\n", resPackages, resBytes);
				resBytes = 0;
				resPackages = 0;
			}

		}

		close(sock);
	}

	close(listen_sock);

	return 0;
}

void printHelp(void)
{
	printf("-p <port>		the port to listen on\n");
	printf("-r <respond>	send the received package back to the sender\n");
	printf("-u 			use UDP instead of TCP\n");
	printf("-h 			prints this help menu\n");
}

int main(int argc, char **argv)
{
	signal(SIGINT, intHandler);

	char* port = "1234";

	int c;
	int respond = 0;
	int udp = 0;
	char *iface = "";


	opterr = 0;

	while ((c = getopt (argc, argv, "hrup:i:")) != -1)
		switch (c) {
		case 'r':
			respond = 1;
			break;
		case 'p':
			port = optarg;
			break;
		case 'i':
			iface = optarg;
			break;
		case 'u':
			udp = 1;
			break;
		case 'h':
			printHelp();
			return 0;
		default:
			abort ();
		}
	printf("ping mode: %d, udp: %d\n", respond, udp);


	if (udp) {
		return receive_udp(port, respond, iface);
	} else {
		return receive_tcp(port, respond, iface);
	}
}
