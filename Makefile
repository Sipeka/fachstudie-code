.PHONY: clean All

All:
	@echo "----------Building project:[ udp-receiver - Release ]----------"
	@cd "udp-receiver" && "$(MAKE)" -f  "udp-receiver.mk"
clean:
	@echo "----------Cleaning project:[ udp-receiver - Release ]----------"
	@cd "udp-receiver" && "$(MAKE)" -f  "udp-receiver.mk" clean
