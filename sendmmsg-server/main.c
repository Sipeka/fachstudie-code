
#define _GNU_SOURCE
#include <stdio.h>

#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>

#include <netinet/in.h>
#include <netinet/ip.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>

#include <arpa/inet.h>
#include <stdlib.h>

#include <signal.h>

#include <net/if.h>

#define min(a,b) \
	({ __typeof__ (a) _a = (a); \
		__typeof__ (b) _b = (b); \
		_a < _b ? _a : _b; })

long getMicrotimeLong(void)
{
	struct timeval currentTime;
	gettimeofday(&currentTime, NULL);
	return currentTime.tv_sec * (int)1e6 + currentTime.tv_usec;
}

/**
 * Returns the current time in microseconds.
 */
void getMicrotime(unsigned char* buf)
{
	struct timeval currentTime;
	gettimeofday(&currentTime, NULL);
	long timestamp = currentTime.tv_sec * (int)1e6 + currentTime.tv_usec;
	buf[0] = timestamp         & 0xFF;
	buf[1] = (timestamp >>  8) & 0xFF;
	buf[2] = (timestamp >> 16) & 0xFF;
	buf[3] = (timestamp >> 24) & 0xFF;
	buf[4] = (timestamp >> 32) & 0xFF;
	buf[5] = (timestamp >> 40) & 0xFF;
	buf[6] = (timestamp >> 48) & 0xFF;
	buf[7] = (timestamp >> 56) & 0xFF;
}


static volatile int keepRunning = 1;

void intHandler(int dummy)
{
	keepRunning = 0;
	exit(0);

}

long cArrayToLong(unsigned char* arr)
{
	return
	    (long)arr[0] |
	    ((long)arr[1] <<  8) |
	    ((long)arr[2] <<  16) |
	    ((long)arr[3] <<  24) |
	    ((long)arr[4] <<  32) |
	    ((long)arr[5] <<  40) |
	    ((long)arr[6] <<  48) |
	    ((long)arr[7] <<  56);
}

int create_socket(int port, char* host, char* iface, int udp)
{
	int sockfd;
	struct sockaddr_in sa;

	sockfd = socket(udp ? AF_INET : PF_INET, udp ? SOCK_DGRAM : SOCK_STREAM, 0);
	if (sockfd == -1) {
		perror("socket()");
		exit(EXIT_FAILURE);
	}

	if (strcmp(iface, "") != 0) {

		struct ifreq ifr;

		memset(&ifr, 0, sizeof(ifr));
		snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), iface);
		if (setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE, (void *)&ifr, sizeof(ifr)) < 0) {
			perror("error binding socket to interface");
		}
	}

	struct timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = 100000;
	if (setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO,&tv,sizeof(tv)) < 0) {
		perror("Error");
	}

	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = inet_addr(host);
	sa.sin_port = htons(port);

	if (connect(sockfd, (struct sockaddr *) &sa, sizeof(sa)) == -1) {
		perror("connect()");
		return EXIT_FAILURE;
	}

	return sockfd;
}

int send_messages_udp(int pkg_size, int pkg_count, int port, char* host, char* iface, int r, int duration_in_s)
{
	int sockfd = create_socket(port, host, iface, 1);
	struct mmsghdr msg[pkg_count];
	struct iovec msg_iovec[pkg_count];
	int retval;
	struct sockaddr_storage src_addr;
	socklen_t src_addr_len=sizeof(src_addr);

	memset(msg_iovec, 0, sizeof(msg_iovec));
	memset(msg, 0, sizeof(msg));

	unsigned char time[8];
	unsigned char rcv_time[8];
	getMicrotime(time);
	for(int i = 0; i < pkg_count; i++) {
		unsigned char buf[pkg_size];
		for(int n = 0; n < pkg_size; n++) {
			if (n < 8) {
				buf[n] = time[n];
			} else {
				buf[n] = '_';
			}
		}
		msg_iovec[i].iov_base = buf;
		msg_iovec[i].iov_len = pkg_size;
		msg[i].msg_hdr.msg_iov = &msg_iovec[i];
		msg[i].msg_hdr.msg_iovlen = 1;
	}


	long lastReportTime = getMicrotimeLong();
	long sentBytes = 0;
	long sentPackages = 0;
	long pingTimeTotal = 0;
	long countPings = 0;

	int interval = (int)1e6;
	long sentSize = pkg_size * pkg_count;
	char rcv_buffer[1549];

	int ticks = 0;

	while (keepRunning) {
		long currTime = getMicrotimeLong();
		if (currTime - lastReportTime > interval) {
			lastReportTime += interval;

			printf("%f Mpps, and %f GBps. Detail: ", sentPackages / (double)1e6, sentBytes * 8 / (double)1e9);
			printf("%ld packages and %ld bytes sent\n", sentPackages, sentBytes);
			sentBytes = 0;
			sentPackages = 0;

			if (r) {
				long avgPingTime = pingTimeTotal / countPings;
				printf("avg ping time: %f ms.\n\n", avgPingTime / 1000.0);
			}

			if(duration_in_s > 0) {
				ticks++;
				if (ticks > duration_in_s) {
					break;
				}
			}
			countPings = 0;
			pingTimeTotal = 0;
		}
		for (int i = 0; i < 100; i++) {
			if (r) {
				// set the systemtime before every send call
				getMicrotime(time);
				for(int i = 0; i < pkg_count; i++) {
					memcpy(msg_iovec[i].iov_base, time,  min(sizeof time, pkg_size));
				}
			}
			retval = sendmmsg(sockfd, msg, pkg_count, 0);
			sentBytes += sentSize;
			sentPackages += retval;
			if (retval == -1)
				perror("sendmmsg()");
			if (r)
				for (int n = 0; n < pkg_count; n++) {
					// receive packages from the server responce
					ssize_t count = recvfrom(sockfd,rcv_buffer,sizeof(rcv_buffer),0,(struct sockaddr*)&src_addr,&src_addr_len);

					if (count >= 8) {
						long currTime = getMicrotimeLong();
						memcpy(rcv_time, rcv_buffer,  sizeof rcv_time);
						long rt = cArrayToLong(rcv_time);
						long ping_time = currTime - rt;
						pingTimeTotal += ping_time;
						countPings++;
						// read the time and compare to now;
					} else {
						pingTimeTotal += 100000;
						countPings++;
					}
				}
		}
	}

	close(sockfd);

	return 0;
}

int send_all(int socket, void *buffer, size_t length)
{
	char *ptr = (char*) buffer;
	while (length > 0) {
		int i = send(socket, ptr, length, 0);
		if (i < 1) return 0;
		ptr += i;
		length -= i;
	}
	return 1;
}

int send_messages_tcp(int pkg_size, int port, char* host, char* iface, int r, int duration_in_s)
{
	int sockfd = create_socket(port, host, iface, 0);

	long lastReportTime = getMicrotimeLong();
	long sentBytes = 0;
	long sentPackages = 0;
	long pingTimeTotal = 0;
	long countPings = 0;
	int retval;


	unsigned char time[8];
	unsigned char rcv_time[8];

	int interval = (int)1e6;
	char send_buffer[pkg_size];
	char rcv_buffer[pkg_size];
	char in_buffer[pkg_size];
	printf("established tcp socket\n");

	int ticks = 0;


	while (keepRunning) {
		long currTime = getMicrotimeLong();
		if (currTime - lastReportTime > interval) {
			lastReportTime += interval;
			printf("%f Mpps, and %f GBps. Detail: ", sentPackages / (double)1e6, sentBytes * 8 / (double)1e9);
			printf("%ld tcp packages and %ld bytes sent\n", sentPackages, sentBytes);
			sentBytes = 0;
			sentPackages = 0;

			if (r) {
				long avgPingTime = pingTimeTotal / countPings;
				printf("tcp avg ping time: %f ms.\n\n", avgPingTime / 1000.0);
			}

			if(duration_in_s > 0) {
				ticks++;
				if (ticks > duration_in_s) {
					break;
				}
			}

			countPings = 0;
			pingTimeTotal = 0;
		}
		for (int i = 0; i < 100; i++) {
			if (r) {
				// set the systemtime before every send call
				getMicrotime(time);
				memcpy(send_buffer, time,  min(sizeof time, pkg_size));
			}

			retval = send_all(sockfd, send_buffer, pkg_size);
			sentBytes += pkg_size;
			sentPackages++;

			if (retval == -1)
				perror("send tcp\n");
			if (r) {
				int n = 0;
				int len = 0;
				char *p_rcv_buffer = rcv_buffer;

				while (len < pkg_size && (n = recv(sockfd, p_rcv_buffer, sizeof p_rcv_buffer, 0)) > 0) {
					memcpy(&in_buffer[len], rcv_buffer, n);
					len += n;

					sentBytes += n;
					sentPackages++;
				}
				if (len >= 8) {
					long currTime = getMicrotimeLong();
					memcpy(rcv_time, in_buffer,  sizeof rcv_time);
					long rt = cArrayToLong(rcv_time);
					long ping_time = currTime - rt;
					pingTimeTotal += ping_time;
					countPings++;
					// read the time and compare to now;
				} else {
					perror("could not receive udp package from client");
				}

			}
		}
	}

	close(sockfd);


	printf("exit send tcp with keepRunning = %d\n", keepRunning);

	return 0;
}


void printHelp(void)
{
	printf("-p <port>		the port to listen on\n");
	printf("-n <number>		the number of messages send by sendmmsg()\n");
	printf("-l <length>		length of the packages (pkg size)\n");
	printf("-s <server>		server address: the host name to send to\n");
	printf("-i <interface>	interface to send over\n");
	printf("-u 				use UDP instead of TCP\n");
	printf("-r 				receive a respond from server after sending\n");
	printf("-h 				prints this help menu\n");
}


int main(int argc, char **argv)
{
	int pkg_size = 60;
	int pkg_count = 10;

	char *host = "127.0.0.1";
	int port = 1234;
	char *iface = "";

	int c;
	int r = 0;
	int udp = 0;

	opterr = 0;
	signal(SIGINT, intHandler);


	while ((c = getopt (argc, argv, "hrul:n:s:p:i:")) != -1)
		switch (c) {
		case 'l':
			if(sscanf (optarg, "%i", &pkg_size) != 1) {
				fprintf(stderr, "error - not an integer");
				pkg_size = 20;
			}
			break;
		case 'n':
			if(sscanf (optarg, "%i", &pkg_count) != 1) {
				fprintf(stderr, "error - not an integer");
				pkg_count = 20;
			}
			break;
		case 'p':
			if(sscanf (optarg, "%i", &port) != 1) {
				fprintf(stderr, "error - not an integer");
				port = 1234;
			}
			break;
		case 'i':
			iface = optarg;
			break;
		case 's':
			host = optarg;
			break;
		case 'u':
			udp = 1;
			break;
		case 'r':
			r = 1;
			break;
		case 'h':
			printHelp();
			return 0;
		default:
			abort ();
		}


	printf("start sending with payload of %d and %d packages per sendmmsg\n", pkg_size, pkg_count);
	printf("ping mode: %d, udp: %d, interface: %s\n", r, udp, iface);
	if (pkg_size > 0 && pkg_count > 0) {
		if (udp) {
			return send_messages_udp(pkg_size, pkg_count, port, host, iface, r, -1);
		} else {
			return send_messages_tcp(pkg_size, port, host, iface, r, -1);
		}
	} else {
		printf("Entering benchmark mode.. looping.\n");

		for (int p_count = 1; p_count < ((udp > 0) ? 50 : 2); p_count *= 2) {
			printf("\n%d sent requests per sendmmsg():\n", p_count);
			for (int p_size = 32; p_size <= 2048; p_size *= 2) {
				printf("\nbenchmark with package size: %d, \n", p_size);
				if (udp)
					send_messages_udp(p_size, p_count, port, host, iface, r, 5);
				else
					send_messages_tcp(p_size, port, host, iface, r, 5);

			}

		}
	}

	return 0;
}
